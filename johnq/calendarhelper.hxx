#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <locale>

using namespace std;

struct ArgsCalendar{
	int monthInitial;
	int monthFinal;
	int year;
	string cmd;
};

void monthFirstDay(tm* t);

void printCalendar(tm* pTm, int daysMonth);

// Read command arguments, it has a reference to an struct to set the month, month range, year.
// and the operation to perform is deduced from the members of the structure.
// Returns false if the arguments can't be interpreted.
bool par_reader(int argc, char** pString, ArgsCalendar& A);

bool is_leap(int year);

int numdaysmonth(int _month, bool is_leap);