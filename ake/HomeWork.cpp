// Home Work : Write a C++ program that prints out a calendar for a given month in the following style :
// April 2018
//	Mon	Tue	Wed	Thu	Fri	Sat	Sun
//							 1
//	 2	 3	 4	 5	 6	 7   8
//	 9	10  11  12  13  14  15
//	16  17  18  19  20  21  22
//	23  24  25  26  27  28  29
//	30
//
// Command - line arguments :
//  No program argument : Print calendar for current month
//  Month Name			: Print calendar for given month in current year
//  Month and year		: Print calendar for given month in given year
//
// Extra :
//  Start week on Sunday(or any other week - day if you like)
//  Print more than one month, if given an interval as input
//  Set language, such as Swedish week - day and month names.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
#include <vector>

using namespace std;

const vector<string> Weekdays = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
const vector<string> WeekdayAcronyms = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
const vector<string> MonthNames = { "January", "February", "March", "April", "May", "June", 
									"July", "August", "September", "October", "November", "December" };
const vector<string> MonthAcronyms = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
const vector<int> DaysPerMonth = { 31,28,31,30,31,30,31,31,30,31,30,31 };

	struct date {
		int Year, Month, Day;
		string getMonthName() {
			return MonthNames[Month - 1];
		}
		void Today() {
			Year = 2018; Month = 05; Day = 1;

			// gets a time-stamp in seconds, converting it to the format: Www Mmm dd hh:mm:ss yyyy\n
			time_t result = time(nullptr);
			char TimeStr[26];
			ctime_s(TimeStr, sizeof TimeStr, &result);
			string TempString;

			// Grabs the year, i.e. the 4 last chars.
			TempString = "";
			for (int i = 20; i < 24; i++) { TempString += TimeStr[i]; }
			Year = stoi(TempString);

			// Grabs the month, i.e. the 3 chars after the weekday acronym
			TempString = "";
			for (int i = 4; i < 7; ++i) { TempString += TimeStr[i]; }
			for (int i = 0; i < 12; ++i) { if (TempString == MonthAcronyms[i]) { Month = i + 1; break; } }

			// Grabs the day, i.e. the 2 chars after the month acronym
			TempString = "";
			for (int i = 8; i < 10; ++i) { TempString += TimeStr[i]; }
			Day = stoi(TempString);

			return;
		}

	};

	bool LeapYear(int Year) {
		// checks if a given year is a leap-year or not
		if (Year % 400 == 0) { return true; }
		else if (Year % 100 == 0) { return false; }
		else if (Year % 4 == 0) { return true; }
		else { return false; }
	}

	int GetWeekday(date d) 
		// calculates out the weekday by converting the date into days and then do a %7 operation
		int NumberOfDays = 0;		
		NumberOfDays = (d.Year - 1) * 365;
		NumberOfDays += (d.Year -1) / 4;
		NumberOfDays -= (d.Year -1) / 100;
		NumberOfDays += (d.Year -1) / 400;
		for (int i = 0; i < d.Month - 1; ++i) {
			NumberOfDays += DaysPerMonth[i];
			if (i == 1 && d.Year % 4 == 0 && ((d.Year % 100 > 0) || (d.Year % 400 == 0))) NumberOfDays++;
		}
		return (NumberOfDays%7) + 1;
	}

void WriteOutCalendar(date CalendarMonth) {
	// Currently writes out the calendar in the standard format. 
	cout << MonthNames[CalendarMonth.Month-1] << " " << CalendarMonth.Year << endl;
	for (int i = 0; i < 7; ++i) { cout << WeekdayAcronyms[i] << '\t'; }
	cout << endl;
	date tempDate = CalendarMonth;
	tempDate.Day = 1;
	int MonthStart = GetWeekday(tempDate);
	int CalendarDay = 1;
	for (int j = 0; j < 7; ++j) {
		if (MonthStart <= j+1 ) {
			cout << " " << CalendarDay << '\t';
			++CalendarDay;
		}
		else { cout << '\t'; }
	}
	cout << endl;
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 7; ++j) {
			if (CalendarDay > 9) { cout << CalendarDay; }
			else { cout << " " << CalendarDay; }
			cout << '\t';
			++CalendarDay;
			if ((CalendarMonth.Month == 2) && (LeapYear(CalendarMonth.Year))) {
				if (CalendarDay > (DaysPerMonth[CalendarMonth.Month - 1] + 1)) {
					return;
				}
			}
			else if (CalendarDay > DaysPerMonth[CalendarMonth.Month - 1]) { return; }
		}
		cout << endl;
	}	
	return;
}


int main(int argi, char* argv[])
{	
	date CalenderDate;
	CalenderDate.Today(); // default
	if (argi>1) {
		for (int i = 0; i < 12; ++i) {
			if (argv[1] == MonthAcronyms[i]) { CalenderDate.Month = i + 1; break; }
			if (argv[1] == MonthNames[i]) { CalenderDate.Month = i + 1; break; }
		}
		if (argi > 2) {
			CalenderDate.Year = stoi(argv[2]);
		}
	}
		
	WriteOutCalendar(CalenderDate);
	cout << endl;

    return 0;
}