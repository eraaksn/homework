#pragma once
#include <string>
#include <vector>

using namespace std;

class Language {

private:
    string languageCode;
    vector<string> monthNames;
    vector<string> weekDays;

public:
    Language() = default;
    Language(string code);
    ~Language() = default;

    void setMonths(const vector<string>& m);
    void setWeeks(const vector<string>& s);

    string getCode() const;
    string getMonth(int monthNo) const;
    unsigned getMonthNo(string monthName) const;
    unsigned getWeekDayNo(string weekDayName) const;
    string getWeekDay(int wDNo) const;

    friend Language getLanguage(vector<Language> languages, string code);
};

vector<Language> createLanguages();

