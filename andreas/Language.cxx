#include <iostream>
#include "Language.hxx"


// Constructors
Language::Language(string code) : languageCode(move(code)) {}


// Setters
void Language::setMonths(const vector<string>& m) {
    for (const string& str : m) { monthNames.push_back(str); }
}

void Language::setWeeks(const vector<string>& w) {
    for (const string& str : w) { weekDays.push_back(str); }
}


// Getters
string Language::getCode() const {
    return languageCode;
}

string Language::getMonth(int monthNo) const {
    return monthNames[monthNo];
}

unsigned Language::getMonthNo(string monthName) const {

    for (auto m = 0U; m <= 11; ++m) {
        if (monthNames[m] == monthName) { return m; }
    }

    return 0U;
}

unsigned Language::getWeekDayNo(string weekDayName) const {

    for (auto w = 0U; w <= 6; ++w) {
        if (weekDays[w] == weekDayName) { return w; }
    }

    return 0U;
}

string Language::getWeekDay(int wDNo) const {
    return weekDays[wDNo];
}


// Help functions
Language getLanguage(vector<Language> languages, string code) {

    for (Language lang : languages) {
        if (lang.languageCode == code) { return lang; }
    }

    // Return default=eng
    for (Language lang : languages) {
        if (lang.languageCode == "eng") { return lang; }
    }

    Language empty;
    return empty;
}

vector<Language> createLanguages() {
    vector<Language> languages;

    {
        Language tmpL("eng");
        vector<string> tmpM = {"January"s, "February"s, "March"s, "April"s, "May"s, "June"s, "July"s, "August"s, "September"s, "Oktober"s, "November"s, "December"s};
        tmpL.setMonths(tmpM);
        vector<string> tmpW = {"Sun"s, "Mon"s, "Tue"s, "Wed"s, "Thu"s, "Fri"s, "Sat"s};
        tmpL.setWeeks(tmpW);
        languages.push_back(tmpL);
    }

    {
        Language tmpL("swe");
        vector<string> tmpM = {"Januari"s, "Februari"s, "Mars"s, "April"s, "Maj"s, "Juni"s, "Juli"s, "Augusti"s, "September"s, "Oktober"s, "November"s, "December"s};
        tmpL.setMonths(tmpM);
        vector<string> tmpW = {"Sön"s, "Mån"s, "Tis"s, "Ons"s, "Tor"s, "Fre"s, "Lör"s};
        tmpL.setWeeks(tmpW);
        languages.push_back(tmpL);
    }

    {
        Language tmpL("esp");
        vector<string> tmpM = {"Enero"s, "Febrero"s, "Marzo"s, "Abril"s, "Mayo"s, "Junio"s, "Julio"s, "Agosto"s, "Septiembre"s, "Oktubre"s, "Noviembre"s, "Dicembre"s};
        tmpL.setMonths(tmpM);
        vector<string> tmpW = {"Dom"s, "Lun"s, "Mar"s, "Mie"s, "Jue"s, "Vie"s, "Sab"s};
        tmpL.setWeeks(tmpW);
        languages.push_back(tmpL);
    }

    {
        Language tmpL("ger");
        vector<string> tmpM = {"Januar"s, "Februar"s, "März"s, "April"s, "Mai"s, "Juni"s, "Juli"s, "August"s, "September"s, "Oktober"s, "November"s, "Dezember"s};
        tmpL.setMonths(tmpM);
        vector<string> tmpW = {"Son"s, "Mon"s, "Die"s, "Mit"s, "Don"s, "Fre"s, "Sam"s};
        tmpL.setWeeks(tmpW);
        languages.push_back(tmpL);
    }

    return languages;
}